<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'email' => 'required | email | unique:users',
            'name' => 'required',
            'password' => 'required | confirmed',
        ]);

        $user = User::create([
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
        ]);

        Auth::login($user);
        return response()->json(['success' => 'Successfully registered'], 200);
    }


    public function login(Request $request){
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

            if (Auth::attempt([
                'email' => $request->email,
                'password' => $request->password,
            ])) {
                return response()->json(['success' => 'Successfully logged in'], 200);
            }else{
                return response()->json(['error' => 'Not logged in'], 404);
            }
                return response()->json(['error' => 'Incorrect login or password'], 401);
    }

    public function logout(){
        Auth::logout();
        return response()->json(['success' => 'Logged out'], 200);
    }
}
